﻿using System.Data.SqlClient;
using SistemaEducativo.Datos.Interfaces;
namespace SistemaEducativo.Datos
{
    public class ConexionDb : IConexionDb
    {

        private readonly string Base = "db_sistema_educativo";
        private readonly string Servidor = "USER\\SQLEXPRESS";
        private readonly string Usuario = "sa";
        private readonly string Clave = "123456";
        private readonly bool Seguridad = false;

        // Inicializa instance aquí
        private static ConexionDb instance = new ConexionDb();
        private ConexionDb()

        { 
        }
        public static IConexionDb GetInstance()

        {
            return instance;
        }
        public SqlConnection CrearConexion()
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = Servidor,
                InitialCatalog = Base,
                IntegratedSecurity = Seguridad
            };
            if (!Seguridad)
            {
                connectionStringBuilder.UserID = Usuario;
                connectionStringBuilder.Password = Clave;
            }
            return new SqlConnection(connectionStringBuilder.ConnectionString);
        }
    }
}