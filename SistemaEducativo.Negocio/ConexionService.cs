﻿using ClassLibrary1SistemaEducativo.Negocio.Interfaces;
using SistemaEducativo.Datos.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1SistemaEducativo.Negocio
{

    public class ConexionService : IConexionService

    {

        private readonly IDbConnectionChecker _connectionChecker;



        public ConexionService(IDbConnectionChecker connectionChecker)

        {

            _connectionChecker = connectionChecker ?? throw new ArgumentNullException(nameof(connectionChecker));

        }



        public bool CheckConnection()

        {

            try

            {

                // Intentar verificar la conexión utilizando el IDbConnectionChecker de la capa de datos

                return _connectionChecker.CheckConnection();

            }

            catch (SqlException ex)

            {

                // Manejar la excepción específica de SQL Server

                //throw new ApplicationException($"Error al conectar a la base de datos: {ex.Message}", ex);

                throw new ConexionException("Error al conectar a la base de datos", ex);

            }

            catch (Exception ex)

            {

                // Manejar otras excepciones

                //throw new ApplicationException($"Error general al conectar a la base de datos: {ex.Message}", ex);

                throw new ConexionException("Error general al conectar a la base de datos", ex);

            }

        }

    }

    // Definir una excepción personalizada para la capa de negocio

    public class ConexionException : ApplicationException

    {

        public ConexionException(string message) : base(message) { }

        public ConexionException(string message, Exception innerException) : base(message, innerException) { }

    }

}
