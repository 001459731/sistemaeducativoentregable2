﻿using SistemaEducativo.Datos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEducativo.Datos
{
    public class DbConnectionChecker : IDbConnectionChecker
    {
        private readonly IConexionDb _conexionDb;
        public DbConnectionChecker(IConexionDb conexionDb)
        {
            _conexionDb = conexionDb ?? throw new ArgumentNullException(nameof(conexionDb));
        }
        public bool CheckConnection()
        {
            try
            {
                // Intentar verificar la conexión utilizando el IDbConnectionChecker de la capa de datos
                using (var conexion = _conexionDb.CrearConexion())
                {
                    conexion.Open();
                    return true; // La conexión fue exitosa
                }
            }
            catch (Exception ex)
            {
                // Relanzar la excepción capturada como una ApplicationException
                throw new ApplicationException($"Error al conectar a la base de datos: {ex.GetType()}, {ex.Message}", ex);
            }
        }
    }
}
