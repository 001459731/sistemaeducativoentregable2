﻿using SistemaEducativo.Entidades;

namespace SistemaEducativo.Negocio.Interfaces
{

    public interface IPersonaService

    {

        Persona ObtenerPorDni(string dni);

        IEnumerable<Persona> ObtenerTodas();

        void Insertar(Persona persona);

        void Actualizar(Persona persona);

    }

}
