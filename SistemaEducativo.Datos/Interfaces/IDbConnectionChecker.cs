﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEducativo.Datos.Interfaces
{
    public interface IDbConnectionChecker
    {
        bool CheckConnection();// Cambiado a método

    }
}
