﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1SistemaEducativo.Negocio.Interfaces
{

    public interface IConexionService

    {

        bool CheckConnection();

    }

}
