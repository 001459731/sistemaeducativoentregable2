﻿using SistemaEducativo.Datos.Interfaces;
using SistemaEducativo.Entidades;
using SistemaEducativo.Negocio.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaEducativo.Negocio;
public class PersonaService : IPersonaService

{

    private readonly IPersonaRepository _personaRepository;



    public PersonaService(IPersonaRepository personaRepository)

    {

        _personaRepository = personaRepository ?? throw new ArgumentNullException(nameof(personaRepository));

    }



    public Persona ObtenerPorDni(string dni)

    {

        if (string.IsNullOrEmpty(dni))

        {

            throw new ArgumentException("El DNI no puede estar vacío", nameof(dni));

        }



        return _personaRepository.ObtenerPorDni(dni);

    }



    public IEnumerable<Persona> ObtenerTodas()

    {

        return _personaRepository.ObtenerTodas();

    }



    public void Insertar(Persona persona)

    {

        if (persona == null)

        {

            throw new ArgumentNullException(nameof(persona));

        }



        _personaRepository.Insertar(persona);

    }



    public void Actualizar(Persona persona)

    {

        if (persona == null)

        {

            throw new ArgumentNullException(nameof(persona));

        }



        _personaRepository.Actualizar(persona);

    }



    /**

    public void Eliminar(Persona persona)

    {

      if (persona == null || string.IsNullOrEmpty(persona.Dni))

      {

        throw new ArgumentException("El DNI no puede estar vacío", nameof(persona));

      }



      _personaRepository.Eliminar(persona);

    }*/

}