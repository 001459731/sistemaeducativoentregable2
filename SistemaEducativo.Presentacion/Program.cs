﻿using SistemaEducativo.Datos.Interfaces;
using SistemaEducativo.Datos;
using SistemaEducativo.Negocio.Interfaces;
using SistemaEducativo.Negocio;
using SistemaEducativo.Entidades;
using ClassLibrary1SistemaEducativo.Negocio.Interfaces;
using ClassLibrary1SistemaEducativo.Negocio;


namespace SistemaEducativo.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instanciar las dependencias necesarias (podrías usar un contenedor de IoC para esto)
            IConexionDb conexionDb = ConexionDb.GetInstance();
            IDbConnectionChecker connectionChecker = new DbConnectionChecker(conexionDb);
            IConexionService conexionService = new ConexionService(connectionChecker);
            IPersonaRepository personaRepository = new PersonaRepository(conexionDb);
            IPersonaService personaService = new PersonaService(personaRepository);

            try
            {
                // Intentar verificar la conexión
                bool isConnected = connectionChecker.CheckConnection();

                // Mostrar mensaje dependiendo del resultado de la conexión
                if (isConnected)
                {
                    Console.WriteLine("Conexión a la base de datos exitosa.");

                    // Crear el menú CRUD
                    var menuCRUD = new MenuCRUD(personaService);

                    // Mostrar el menú y realizar operaciones hasta que el usuario decida salir

                    menuCRUD.MostrarMenu();
                }

                else

                {
                    Console.WriteLine("No se pudo establecer la conexión.");
                }

            }

            catch (ApplicationException ex)
            {
                // Mostrar excepción específica capturada durante la verificación de la conexión

                Console.WriteLine($"Error al conectar a la base de datos: {ex.GetType()}, {ex.Message}");

                // También puedes mostrar información adicional si lo deseas: ex.InnerException, ex.StackTrace, etc.
            }

            catch (Exception ex)

            {

                // Mostrar otras excepciones no específicas capturadas durante la verificación de la conexión

                Console.WriteLine($"Error general al conectar a la base de datos: {ex.GetType()}, {ex.Message}");

                // También puedes mostrar información adicional si lo deseas: ex.InnerException, ex.StackTrace, etc.

            }



            Console.ReadLine();

        }

    }



    // Clase para el menú CRUD

    public class MenuCRUD

    {

        private readonly IPersonaService _personaService;



        public MenuCRUD(IPersonaService personaService)
        {
            _personaService = personaService;
        }

        public void MostrarMenu()
        {
            Console.WriteLine("Menú CRUD:");
            Console.WriteLine("1. Crear");
            Console.WriteLine("2. Leer");
            Console.WriteLine("3. Actualizar");
            Console.WriteLine("4. Eliminar");
            Console.WriteLine("0. Salir");

            Console.Write("Seleccione una opción: ");
            int opcion = int.Parse(Console.ReadLine());

            switch (opcion)
            {
                case 1:

                    CrearPersona();

                    break;

                case 2:

                    LeerPersonas();

                    break;

                case 3:

                    ActualizarPersona();

                    break;

                case 4:

                    //EliminarPersona();

                    break;

                case 0:

                    Console.WriteLine("Saliendo del menú...");

                    return;

                default:

                    Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");

                    break;

            }



            MostrarMenu();

        }



        private void CrearPersona()

        {

            // Capturar datos de la nueva persona

            Console.WriteLine("Ingrese el DNI de la persona:");

            string dni = Console.ReadLine();



            //Console.WriteLine("Ingrese el estado:");

            //string estado = Console.ReadLine();



            Console.WriteLine("Ingrese el estado de la persona (true/false):");

            string estadoString = Console.ReadLine();

            bool estado;

            while (!bool.TryParse(estadoString, out estado))

            {

                Console.WriteLine("Valor inválido. Por favor, ingrese 'true' o 'false':");

                estadoString = Console.ReadLine();

            }



            Console.WriteLine("Ingrese el RUC:");
            string ruc = Console.ReadLine();

            Console.WriteLine("Ingrese los apellidos paterno de la persona:");
            string apellidoPaterno = Console.ReadLine();

            Console.WriteLine("Ingrese los apellidos materno de la persona:");
            string apellidoMaterno = Console.ReadLine();

            Console.WriteLine("Ingrese los nombres de la persona:");
            string nombres = Console.ReadLine();

            Console.WriteLine("Ingrese la edad de la persona:");
            int edad;

            while (!int.TryParse(Console.ReadLine(), out edad))
            {
                Console.WriteLine("Edad inválida. Por favor ingrese un número entero:");
            }

            Console.WriteLine("Ingrese el sexo de la persona (M/F):");

            char sexo;
            while (!char.TryParse(Console.ReadLine(), out sexo) || (sexo != 'M' && sexo != 'F'))
            {
                Console.WriteLine("Sexo inválido. Por favor ingrese 'M' para masculino o 'F' para femenino:");
            }

            Console.WriteLine("Ingrese el correo electrónico de la persona:");
            string email = Console.ReadLine();

            Console.WriteLine("Ingrese la foto de la persona:");
            string foto = Console.ReadLine();

            // Crear objeto Persona con los datos ingresados
            Persona nuevaPersona = new Persona
            {
                Dni = dni,
                Estado = estado,
                Ruc = ruc,
                ApellidoPaterno = apellidoPaterno,
                ApellidoMaterno = apellidoMaterno,
                Nombres = nombres,
                Edad = edad,
                Sexo = sexo,
                Email = email,
                Foto = foto
            };



            // Insertar la nueva persona en la base de datos

            _personaService.Insertar(nuevaPersona);



            Console.WriteLine("La persona se ha agregado correctamente a la base de datos.");

        }



        private void LeerPersonas()

        {

            // Lógica para leer personas desde la base de datos

            var personas = _personaService.ObtenerTodas(); // Ejemplo de método para obtener todas las personas



            // Mostrar las personas por consola

            Console.WriteLine("Personas registradas:");

            foreach (var persona in personas)

            {

                Console.WriteLine($"DNI: {persona.Dni}, Apellidos: {persona.ApellidoPaterno} {persona.ApellidoMaterno}, Nombres: {persona.Nombres}, Edad: {persona.Edad}, Sexo: {persona.Sexo}, Email: {persona.Email}, Foto: {persona.Foto}");

            }

        }



        private void ActualizarPersona()

        {

            // Lógica para actualizar persona en la base de datos

            Console.WriteLine("Ingrese el DNI de la persona a actualizar:");

            string dni = Console.ReadLine();



            // Obtener la persona a actualizar desde la base de datos

            var persona = _personaService.ObtenerPorDni(dni); // Ejemplo de método para obtener persona por DNI



            if (persona != null)

            {

                Console.WriteLine("Ingrese los nuevos datos de la persona:");



                // Capturar datos actualizados de la persona

                Console.WriteLine("Ingrese los apellidos paterno de la persona:");

                string nuevoApellidoPaterno = Console.ReadLine();

                // Capturar otros datos de la persona...



                // Actualizar los datos de la persona

                persona.ApellidoPaterno = nuevoApellidoPaterno;

                // Actualizar otros datos de la persona...



                // Guardar los cambios en la base de datos

                _personaService.Actualizar(persona); // Ejemplo de método para actualizar persona en la base de datos



                Console.WriteLine("La persona se ha actualizado correctamente.");

            }

            else

            {

                Console.WriteLine("No se encontró ninguna persona con el DNI especificado.");

            }

        }



        /**

        private void EliminarPersona()

        {

          // Lógica para eliminar persona de la base de datos

          Console.WriteLine("Ingrese el DNI de la persona a eliminar:");

          string dni = Console.ReadLine();



          // Intenta eliminar la persona de la base de datos

          try

          {

            _personaService.Eliminar(dni);

            Console.WriteLine("La persona se ha eliminado correctamente.");

          }

          catch (Exception)

          {

            Console.WriteLine("No se encontró ninguna persona con el DNI especificado o ocurrió un error al eliminarla.");

          }

        }*/





    }

}